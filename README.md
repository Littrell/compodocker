# compodocker

## Setup

1. Add the Dockerfile to your repository
2. Add the following entry to your docker-compose.yml

    ```yml
    <service>:
      image: mlittrell/compodocker
      container_name: <container name>
      ports:
        - "80"
      volumes:
        - ./:/app
    ```

3. Replace \<service\> with the service name
4. Replace \<container name\> with the container name
5. Run!

    ```
    docker-compose up -d --build
    ```