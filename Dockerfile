FROM node:8

EXPOSE 80

RUN set -x \
    && npm install -g @compodoc/compodoc static-server

COPY ./entrypoint.sh /entrypoint.sh

RUN chmod +x /entrypoint.sh

WORKDIR /app

ENTRYPOINT ["/entrypoint.sh"]

CMD ["src/tsconfig.app.json"]
